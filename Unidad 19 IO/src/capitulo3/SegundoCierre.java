package capitulo3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * En este ejemplo, la secuencia se crea y se cierra en el método readFile(),
 * con printData() procesando el contenido.
 */
public class SegundoCierre {
    public static void main(String[] args) throws IOException {
        SegundoCierre objeto = new SegundoCierre();
        var cadena = "C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/destino.txt";
        objeto.readFile(cadena);
    }

    public void printData(InputStream is) throws IOException {
        int b;
        while ((b = is.read()) != -1) {
            System.out.println((char) b + " = " + b);
        }
    }

    public void readFile(String fileName) throws IOException {
        try (var fis = new FileInputStream(fileName)) {
            printData(fis);
        }
    }

}
