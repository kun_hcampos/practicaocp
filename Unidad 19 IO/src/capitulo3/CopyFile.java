package capitulo3;

import java.io.*;


public class CopyFile {
    public static void main(String[] args) {
        CopyFile objeto = new CopyFile();
        File origen = new File("C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/origen.txt");
        File destino = new File("C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/destino.txt");

        objeto.metodo(origen, destino);
    }

    public void metodo(final File in, final File out) {
        try {
            InputStream entrada = new FileInputStream(in);
            OutputStream salida = new FileOutputStream(out);

            byte[] buf = new byte[1024];
            int len;

            while ((len = entrada.read(buf)) > 0) {
                salida.write(buf, 0, len);
            }

            entrada.close();
            salida.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
