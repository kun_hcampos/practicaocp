package capitulo3;

import java.io.*;

public class CerrarWrapped {
    public static void main(String[] args) throws IOException {
        var origen = "C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/origen.txt";
        var destino = "C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/destino.txt";

        /**
         * Cuando trabaje con un wrapped stream, solo necesita usar close() en el objeto superior.
         * Hacerlo cerrará el stream subyacentes.
         * El siguiente ejemplo es válido y dará como resultado tres llamadas al método close() separadas,
         * pero no es necesario:
          */

//                        try (var fis = new FileOutputStream(cadena); // Unnecessary
//                             var bis = new BufferedOutputStream(fis);
//                             var ois = new ObjectOutputStream(bis)) {
//
//                            ois.writeObject("Hello");
//
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }

        /**
         * En su lugar, podemos confiar en ObjectOutputStream para cerrar BufferedOutputStream y FileOutputStream.
         * Lo siguiente llamará solo a un método close() en lugar de tres:
         */
        try (var ois = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(destino)))) {
            ois.writeObject(origen);
        }

    }
}
