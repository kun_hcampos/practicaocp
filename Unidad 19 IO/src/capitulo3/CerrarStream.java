package capitulo3;

import java.io.FileInputStream;
import java.io.IOException;

public class CerrarStream {
    public static void main(String[] args) {
        try (var fis = new FileInputStream("C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/destino.txt")) {
            System.out.print(fis.read());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
