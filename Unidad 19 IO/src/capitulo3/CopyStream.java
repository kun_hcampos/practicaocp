package capitulo3;

import java.io.*;

/**
 * Los siguientes métodos copyStream() muestran un ejemplo de
 * cómo leer todos los valores de InputStream y Reader
 * y escribirlos en OutputStream y Writer, respectivamente.
 * En ambos ejemplos, se utiliza ‐1 para indicar el final del stream.
 */
public class CopyStream {
    FileInputStream origen = new FileInputStream("C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/origen.txt");
    FileOutputStream destino = new FileOutputStream("C:/Users/LENOVO/Documents/Daniel/Java/OCP/Archivos/destino.txt");

    public CopyStream() throws FileNotFoundException {
    }

    public static void main(String[] args) throws IOException {
        CopyStream copyStream = new CopyStream();
        copyStream(copyStream.origen, copyStream.destino);
    }


    public static void copyStream(final InputStream in, final OutputStream out) throws IOException {
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        }
    }

}
