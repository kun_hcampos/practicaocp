package capitulo1.separadores;


/**
 * Por conveniencia, Java ofrece dos opciones para recuperar el carácter separador local:
 * 1. Una propiedad del sistema
 * 2. Una variable estática definida en la clase File.
 * Los dos ejemplos siguientes generarán el carácter separador para el entorno actual:
 */

public class FIleSeparator {
    public static void main(String[] args) {
        System.out.println("Separador con System.getProperty: " + System.getProperty("file.separator"));
        System.out.println("Separador con java.io.File: " + java.io.File.separator);
    }
}
