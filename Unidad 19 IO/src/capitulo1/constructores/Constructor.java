package capitulo1.constructores;

import java.io.File;

/**
 * En Este Programa vamos a crear tres instancias de la clase File, con la direccion tomada del directorio
 * de Ejemplo.
 */
public class Constructor {
    public static void main(String[] args) {

        var file = new File("C:/Users/Karen/Documents/Daniel/OCP");
        var cadena = "C:/Users/Karen/Documents/Daniel/OCP";

        //Ruta1 con la firma File(String parametro)
        var ruta1 = new File("C:/Users/Karen/Documents/Daniel/OCP/PracticaOCP/Tema 1");
        System.out.println("Constructor 1: "+ruta1);

        //Ruta2 con la firma File(File parametro, String parametro)
        var ruta2 = new File(file, "PracticaOCP/Tema 1");
        System.out.println("Constructor 2: "+ruta2);

        //Ruta3 con la firma File(String parametro, String parametro)
        var ruta3 = new File(cadena, "/PracticaOCP/Tema 1");
        System.out.println("Constructor 3: "+ruta3);
    }
}
