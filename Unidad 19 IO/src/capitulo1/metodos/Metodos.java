package capitulo1.metodos;

import java.io.File;

public class Metodos {
    public static void main(String[] args) {
        //Definimos un directorio donde se encuentra el archivo:  Archivo.txt
        var file = new File("C:/Users/Karen/Documents/Daniel/OCP/PracticaOCP/Tema 1/Archivo.txt");
        var nombre = new File("C:/Users/Karen/Documents/Daniel/OCP/PracticaOCP/Tema 1/nombre.txt");

        //Metodo Exist:
        System.out.println("exists: " + file.exists()); // true if the file exists.

        //Metodo getAbsolutePath: Devuelve un String con la file absoluta del archivo.
        System.out.println("getAbsolutePath: " + file.getAbsolutePath());

        //Metodo getAbsoluteFile: Devuelve un File con la file absoluta del archivo.
        System.out.println("getAbsoluteFile: " + file.getAbsoluteFile());

        //Metodo getName
        System.out.println("getName: " + file.getName());

        //Devuelte un String con la file del archivo Padre, si no existe, entonces devuelve Null.
        System.out.println("getParent: " + file.getParent());

        //Devuelte un File con la file del archivo Padre, si no existe, entonces devuelve Null.
        System.out.println("getParentFile: " + file.getParentFile());

        //Metodo isDirectory: Determina si es un Directorio
        System.out.println("isDirectory: " + file.isDirectory());

        //Metodo isDirectory: Determina si es un Archivo
        System.out.println("isFile: " + file.isFile());

        //Metodo lastModified : Devuelve el número de milisegundos desde la época cuando el archivo se modificó por última vez.
        System.out.println("lastModified: " + file.lastModified());

        //Comprueba si la aplicación puede ejecutar el archivo indicado por este nombre de file abstracto.
        System.out.println("canExecute: " + file.canExecute());

        // Comprueba si la aplicación puede leer el archivo indicado por este nombre de ruta abstracto.
        System.out.println("canRead: "+file.canRead());

        //Comprueba si la aplicación puede escribir el archivo indicado por este nombre de ruta abstracto.
        System.out.println("canWrite :"+file.canWrite());

        //crea una URL con la ruta del archivo o directorio.
        System.out.println("toURI: "+file.toURI());

        //Renombramos el archivo
        System.out.println("Renombrado a Archivo: "+ nombre.renameTo(file));
        System.out.println(file.getName());

        //Renombramos el archivo
        System.out.println("Renombrado a nombre: "+ file.renameTo(nombre));
        System.out.println(file.getName());

        //Borramos el Archivo
        //System.out.println("Borraste el archivo: " + file.delete());

        //Determinamos si el archivo Existe
        System.out.println("Sigue existiendo: " + file.exists());
    }
}
