package capitulo2.charsets;

import java.nio.charset.Charset;

public class Charsets {

    public static void main(String[] args) {

        Charset utf8Charset = Charset.forName ("UTF-8");
        Charset utf16Charset = Charset.forName("UTF-16");
        Charset usAsciiCharset = Charset.forName("ASCII");

        System.out.println("utf8Charset = " + utf8Charset);
        System.out.println("utf16Charset = " + utf16Charset);
        System.out.println("usAsciiCharset = " + usAsciiCharset);

    }
}
