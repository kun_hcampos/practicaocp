package capitulo2.wrappers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Wrapping {
    public static void main(String[] args) {
        Wrapping wrapping = new Wrapping();
        wrapping.lectura();
    }
    public void lectura() {
        try (var br = new BufferedReader(new FileReader("zoodata.txt"))) {
            System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
