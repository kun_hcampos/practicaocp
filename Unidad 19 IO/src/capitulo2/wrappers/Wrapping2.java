package capitulo2.wrappers;

import java.io.*;

public class Wrapping2 {
    public static void main(String[] args) {
        Wrapping2 wrapping = new Wrapping2();
        wrapping.metodo();
    }
    public void metodo() {
        try (var ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("zoo-data.txt")))) {
            System.out.print(ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
