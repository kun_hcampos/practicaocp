//Implementing Functional Interfaces with Lambdas

public class Animal2 {

    private String especie;
    private boolean puedeSaltar;
    private boolean puedeNadar;


    //Constructor
    public Animal2(String nombreEspecie, boolean saltar, boolean nadar) {
        especie = nombreEspecie;
        puedeSaltar = saltar;
        puedeNadar = nadar;
    }

    //Metodos
    public boolean puedeSaltar() {
        return puedeSaltar;
    }

    public boolean puedeNadar() {
        return puedeNadar;
    }

    public String toString() {
        return especie;
    }
}
