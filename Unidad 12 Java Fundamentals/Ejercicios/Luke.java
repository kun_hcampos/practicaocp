public class Luke {
    private String greeting = "Hi";

    protected class Inner extends Anakin implements fraseLuke {
        public int repeat = 3;
        final Integer numero = 1;
        String greeting = "Dentro de Inner";

        final void calcular() {
            System.out.println("calculando suma de 1 + 3" + 3 + numero);
        }

        public void go() {
            for (int i = 0; i < repeat; i++)
                System.out.println(Luke.this.greeting);
        }

        @Override
        public void frase() {
            System.out.println("La fuerza está con nosotros");
            System.out.println("Inner ... " + mensaje);
        }


    }

    public void callInner() {
        Inner inner = new Inner();
        inner.go();
        inner.frase();
        inner.calcular();
    }

    public static void main(String[] args) {
        Luke outer = new Luke();
        outer.callInner();

        //Otra forma de instancia a Inner

        //Inner inner = new Luke().new Inner();
        //ahora podemos acceder a los metodos que hay en inner class
        //inner.go();
    }
}