public class Switch {
    public static void main(String[] args) {
        Season summer = Season.SUMMER;

        switch (summer) {
            case WINTER:
                System.out.println("Invierno");

            case SUMMER:
                System.out.println("Sol, playa y arena!");

            default:
                System.out.println("¿Tiempo de verano?");
        }


    }
}
