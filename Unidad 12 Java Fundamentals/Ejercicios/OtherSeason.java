public class OtherSeason {
    public static void main(String[] args) {
        Season s = Season.SUMMER;
        System.out.println(Season.SUMMER);
        System.out.println(s.equals(Season.SUMMER));

        for (Season season : Season.values()) {
            System.out.println(season.getExpectedVisitors());
            System.out.println(season.name() + " /// " + season.ordinal());
        }

        Season s1 = Season.valueOf("SUMMER"); // SUMMER
        //Season t = Season.valueOf("summer"); // Throws an exception at runtime


        Season.SUMMER.printExpectedVisitors();

    }
}
