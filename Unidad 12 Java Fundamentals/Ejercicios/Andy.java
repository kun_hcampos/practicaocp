public class Andy {

    private Integer juguete = 10;

    class Jessie {

        private Integer juguete = 20;

        class Woody {

            private Integer juguete = 30;

            public void cantidadJuguetes() {

                System.out.println(juguete);

                System.out.println(this.juguete);

                System.out.println(Jessie.this.juguete);

                System.out.println(Andy.this.juguete);

            }
        }
    }

    public static void main(String[] args) {
        Andy andy = new Andy();
        Andy.Jessie jessie = andy.new Jessie();
        Andy.Jessie.Woody woody = jessie.new Woody();
        woody.cantidadJuguetes();
    }
}
