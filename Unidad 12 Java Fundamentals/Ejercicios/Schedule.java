public interface Schedule {

    default void wakeUp() {
        checkTime(7);
    }

    default void haveBreakfast() {
        checkTime(9);
    }

    default void haveLunch() {
        checkTime(12);
    }

    default void workOut() {
        checkTime(18);
    }

    private void checkTime(int hour) {
        if (hour > 17) {
            System.out.println("¡Lo siento llegas llegas tarde!");
        } else {
            System.out.println("Tienes " + (17 - hour) + " horas para llegar a tiempo.");
        }
    }
}
