public interface Dash extends Sprint {
}

interface Skip extends Sprint {
    void skip();
}

interface Sleep {
    private void snore() {
    }

    default int getZzz() {
        return 1;
    }
}

interface Climb {
    void reach();

    default void fall() {
    }

    static int getBackUp() {
        return 100;
    }

    private static boolean checkHeight() {
        return true;
    }
}