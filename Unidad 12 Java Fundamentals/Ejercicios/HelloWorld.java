class A {
    public final int x;

    A(int arg) {
        x = arg;
    }
}

public class HelloWorld {

    public static void main(String[] args) {
        A a = new A(1);
        A b = new A(2);
        System.out.printf("a.x = %d, b.x = %d", a.x, b.x);
    }
}
