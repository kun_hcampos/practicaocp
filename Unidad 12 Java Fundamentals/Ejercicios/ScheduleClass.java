public class ScheduleClass implements Schedule {

    public static void main(String[] args) {

        ScheduleClass scheduleClass = new ScheduleClass();
        scheduleClass.wakeUp();
        scheduleClass.haveBreakfast();
        scheduleClass.haveLunch();
        scheduleClass.workOut();
    }
}
