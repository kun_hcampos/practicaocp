interface Walk {
    public default int getSpeed() {
        return obtenerNumero();
    }

    default int obtenerNumero(){return 55;}

}

interface Run {
    public default int getSpeed() {
        return 10;
    }
    //abstract int getSpeed();
}

//Comentamos porque necesita implementar el método.
/*public class Cat implements Walk, Run {
    public static void main(String[] args) {
        System.out.println(new Cat().getSpeed());
    }
}*/