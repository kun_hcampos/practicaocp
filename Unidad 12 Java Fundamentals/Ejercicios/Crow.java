public class Crow {

//    private String color;
//
//    public void caw(String name) {
//        String volume = "loudly";
//        Predicate<String> p = s -> (name + volume + color).length() == 10;
//    }

    /*
    En este ejemplo, a los valores de nombre y volumen
    se les asignan nuevos valores en las líneas 16 y 17.
    Por esta razón, la expresión lambda declarada en las líneas 13 y 14
    no se compila ya que hace referencia a variables locales que no son finales
    o efectivamente finales. Si se eliminaran las líneas 15,16 y 17, la clase compilaría.
     */

    private String color;

    public static void caw(String name) {
        String volume = "loudly";

//        color = "allowed";
//        name = "not allowed";
//        volume = "not allowed";
        Predicate<String> p = (final String s) -> {return (s).length() == 2;};
        System.out.println(p.verificarAnimal("10"));

    }

    public static void main(String[] args) {
        caw("");
    }
}