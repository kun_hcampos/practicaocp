public interface IsWarmBlooded {
    boolean hasScales();

    default double getTemperature() {
        return 10.0;
    }

    static double getTemperature2(){
        return 0.0;
    }
}
