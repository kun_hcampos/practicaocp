import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class TraditionalSearch {

    public static void main(String[] args) {
        var animals = new ArrayList<Animal2>();
        animals.add(new Animal2("pescado", false, true));
        animals.add(new Animal2("kangaroo", true, true));
        animals.add(new Animal2("conejo", true, false));
        animals.add(new Animal2("tortuga", false, true));

        //verificar animales que pueden saltar sin necesidad de añadir una clase adicional.
        Consumer<Animal2> animal2Consumer = s -> {};
        Supplier<Animal2> animal2Supplier = () -> {return null;};
        //Comparator<String> stringComparator = (x,var y) -> 0;

        Predicate<Animal2> animal2Predicate = animal2 -> animal2.puedeNadar();

        imprimirAnimal(animals, animal2Predicate);


        //verificar animales que pueden nadar.
        imprimirAnimal(animals, b -> b.puedeNadar());

        //verificar animales que no pueden nadar.
        imprimirAnimal(animals, b -> !b.puedeNadar());

    }

    //Este metodo puede verificar cualquier rasgo que queramos
    private static void imprimirAnimal(List<Animal2> animals, Predicate<Animal2> check) {
        for (Animal2 animal2 : animals) {
            if (check.verificarAnimal(animal2))
                System.out.println("Animal  : " + animal2);
        }
        System.out.println(" ----------------------------------");
    }
}
