public class TestMadera {
    public static void main(String[] args) {

        TipoMadera tm = TipoMadera.ROBLE;

        System.out.println("Madera elegida " + tm + ", con el color " +
                tm.getColor() + " con un peso de " + tm.getPesoEspecifico() + " kg");

        for (TipoMadera tmp : TipoMadera.values()) {
            System.out.println(tmp.toString() + " el palet pesa " + (2.27f * (float) tmp.getPesoEspecifico()) + " kg");
        }
    }
}
