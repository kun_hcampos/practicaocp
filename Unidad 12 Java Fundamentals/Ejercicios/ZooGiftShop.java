public class ZooGiftShop {

//    abstract class SaleTodayOnly {
//        abstract int dollarsOff();
//
//        abstract int cordobaOff();
//    }
//
//    public int admission(int basePrice) {
//        SaleTodayOnly sale = new SaleTodayOnly() {
//            int dollarsOff() {
//                return 3;
//            }
//
//            int cordobaOff() {
//                return 12;
//            }
//        }; //TODO:  No olvidar el ;
//        return basePrice - sale.dollarsOff();
//    }


    //Ahora extendiendo de una interfaz

//    interface SaleTodayOnly {
//        int dollarsOff();
//    }
//
//    public int admission(int basePrice) {
//        SaleTodayOnly sale = new SaleTodayOnly() {
//            public int dollarsOff() {
//                return 3;
//            }
//        }; //No olvidar el ;
//        return basePrice - sale.dollarsOff();
//    }


    //Extendiendo de un argumento de un constructor
    interface SaleTodayOnly$ {
        int dollarsOff();
    }

    public int pay() {
        new SaleTodayOnly$() {
            @Override
            public int dollarsOff() {
                return 0;
            }
        };

        return admission(5, new SaleTodayOnly$() {
            public int dollarsOff() {
                return 3;
            }
        });
    }

    public int admission(int basePrice, SaleTodayOnly$ sale) {
        return basePrice - sale.dollarsOff();
    }


}
