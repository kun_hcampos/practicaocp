public enum TipoMadera {

    ROBLE("Castaño verdoso", 800),
    CAOBA("Marrón oscuro", 770),
    NOGAL("Marrón rojizo", 820),
    CEREZO("Marrón cereza", 790),
    BOJ("Marrón negruzco", 675);

    private final String color;

    private final int pesoEspecifico;

    /**
     * Constructor. Al asignarle uno de los valores posibles a una variable del tipo enumerado el constructor asigna
     * automáticamente valores de los campos
     */

    TipoMadera(String color, int pesoEspecifico) {
        this.color = color;
        this.pesoEspecifico = pesoEspecifico;
    }


    public String getColor() {
        return color;
    }

    public int getPesoEspecifico() {
        return pesoEspecifico;
    }


}
