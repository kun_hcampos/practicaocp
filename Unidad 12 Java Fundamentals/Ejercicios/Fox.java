public class Fox {
    private class Den {
    }

    public void goHome() {
        new Den();
    }

    public static void visitFriend() {
        new Fox().goHome();
        //    new Den(); // No Compila porque se tiene que dar explicitamente una referenia de Fox
        //this.goHome();
    }
}

class Squirrel {
    public void visitFox() {
        //new Fox().goHome(); si extendemos podemos referencias el método instanciandolo.
        //new Den(); // No compilar porque estamos instanciando el método en otra clase
    }
}