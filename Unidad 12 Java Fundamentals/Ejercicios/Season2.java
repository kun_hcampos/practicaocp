public enum Season2 {

    WINTER {
        @Override
        String getHours() {
            return "10am - 3pm";
        }
    },

    SPRING {
        @Override
        public String getHours() {
            return "9am-5pm";
        }
    },

    SUMMER {
        @Override
        public String getHours() {
            return "9am-7pm";
        }
    },

    FALL {
        @Override
        public String getHours() {
            return "9am-5pm";
        }

        @Override
        public String toString() {
            return super.toString() + " //2";
        }
    };

    abstract String getHours();

    public String toString() {
        return this.getHours() + super.toString();
    }



}
