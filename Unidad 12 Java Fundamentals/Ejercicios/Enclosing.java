public class Enclosing {

    static class Nested {
        private int price = 6;

        static class Allen {
            private int bota = 2;
            private static int zapato = 12;
        }


    }

    public static void main(String[] args) {
        Nested nested = new Nested();
        System.out.println(nested.price);

        Enclosing.Nested.Allen allen = new Nested.Allen();
        System.out.println(allen.bota);
        System.out.println(allen.zapato); // by Prof. Hilario Class
    }
}

