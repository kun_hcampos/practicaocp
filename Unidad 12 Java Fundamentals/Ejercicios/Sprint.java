@FunctionalInterface
public interface Sprint {
    public void sprint(int speed);

    //abstract void sprint(int speed);

    //int toString(); //No compila porque la version de Object devuelve un String

}
