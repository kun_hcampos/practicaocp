public enum Season {

    WINTER("LOW"),
    SPRING("MEDIUM"),
    SUMMER("HIGH"),
    FALL("MEDIUM");

    private final String expectedVisitors;

    public String getExpectedVisitors() {
        return expectedVisitors;
    }

    Season(String expectedVisitors) {
        this.expectedVisitors = expectedVisitors;
    }

    public void printExpectedVisitors() {
        System.out.println(expectedVisitors);
    }
}