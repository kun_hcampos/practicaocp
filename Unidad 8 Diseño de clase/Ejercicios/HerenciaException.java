import java.io.FileNotFoundException;
import java.io.IOException;

class HerenciaException {

    public static void main(String[] args) {
        new Prueba2().hola();
    }


}

class Prueba {

    void hola() throws FileNotFoundException {

    }
}

class Prueba2 extends Prueba {

    void hola() throws IOException {
        System.out.println("excception prueba 2");
    }
}