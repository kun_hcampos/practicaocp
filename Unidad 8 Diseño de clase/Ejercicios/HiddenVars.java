public class HiddenVars {


    public static void main(String[] args) {
        BC b = new BC();
        System.out.println(b.hasFur);
        System.out.println(b.x);
        System.out.println(b.y);
        
        AC a = new AC();
        System.out.println(a.hasFur);
        System.out.println(a.x);

        AC ab = new BC();
        System.out.println(b.hasFur);
        System.out.println(ab.hasFur);

    }


}


class AC {
    public boolean hasFur = false;
    public int x = 0;
}


class BC extends AC {
    public boolean hasFur = true;
    public int y = 0;

}