class Sobrecarga1 {

    public static void main(String[] args) {
        new Hijo().flySon();
    }

}

class Padre {

    private void fly() {
        System.out.println("Vuela");
    }
}

class Hijo extends Padre {

    public void flySon() {
        fly();
    }

    private void fly() {
        System.out.println("Vuela");
    }
}