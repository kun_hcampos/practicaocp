/**
 * Polimorfismo
 */
public class Main {
    public static void main(String[] args) {
        Padre padre = new Hijo();
        padre.hola(12);
    }
}

class Padre {
    public void hola(int x) {
        System.out.println("Padre dice Hola" + x);
    }
    public void hola() {
        System.out.println("Padre dice Hola");
    }
}
class Hijo extends Padre {
    public void hola() {
        System.out.println("Hijo dice Hola");
    }
}