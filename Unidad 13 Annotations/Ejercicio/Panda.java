import java.lang.annotation.Documented;
import java.lang.annotation.Target;

public @interface Panda {
    Integer enteroPrueba = 7;
    int height() default enteroPrueba; 				//No compila.
    int[] generalInfo(); 			//No compila.
    Class<Bear> friendlyBear() default Bear.class; 				//No compila.
    Size size() default Size.SMALL;
    //Panda exercise() default @Panda(); //No permite redundancia ciclica
    String value();
    int enteroObligatorio();
}

class Bear {}

@Documented
enum Size {SMALL, MEDIUM, LARGE}

@interface Exercise {

    int hoursPerDay() default 3;
    int startHour() default 6;
}
@Panda("", enteroObligatorio = 1)
class TestValue {}