package ejemplos.collections;

import java.util.Comparator;
import java.util.TreeSet;

public class Sorted implements Comparable<Sorted>, Comparator<Sorted> {
    private int id;
    private String name;

    public Sorted(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int compareTo(Sorted sorted) {
        return this.id - sorted.id;
    }

    @Override
    public int compare(Sorted t1, Sorted t2) {
        return t1.name.compareTo(t2.name);
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", name='" + name + '\'' + '}';
    }

    public static void main(String[] args) {
        Sorted s1 = new Sorted(2, "Batman");
        Sorted s2 = new Sorted(1, "Superman");
        Sorted s3 = new Sorted(3, "Flash");

        var t1 = new TreeSet<Sorted>();
        t1.add(s1);
        t1.add(s2);
        t1.add(s3);

        var t2 = new TreeSet<>(s1);
        t2.add(s1);
        t2.add(s2);
        t2.add(s3);

        System.out.println(t1);
        System.out.println(t2);
    }
}