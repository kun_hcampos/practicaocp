package ejemplos.generics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class UpperBounded {
    public static void main(String[] args) {
        //MAXIMO TIPO ES IOEXCEPTION
        ArrayList< ? super IOException> list = new ArrayList<Exception>();
        list.add(new FileNotFoundException());
        list.add(new IOException());
//        list.add(new Exception());
    }
}
