package ejemplos.generics;

import java.util.ArrayList;
import java.util.List;

public class UnBounded {
    public static void main(String[] args) {
        //  UMBOUNDED WILDCARDS
        print(List.<Integer>of(1, 2, 3, 4, 5));
        print(List.<String>of("Hola", "a", "todos", ":)"));

        List<?> list = new ArrayList<>();
//        list.add("");
//        list.add(1);
        list.add(null);
        list.add(null);
        System.out.println(list);
    }

    static void print(List<?> list) {
        list.forEach(System.out::println);
    }
}
