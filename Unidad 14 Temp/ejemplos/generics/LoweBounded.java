package ejemplos.generics;

import java.util.ArrayList;
import java.util.List;

public class LoweBounded {
    public static void main(String[] args) {
        List<? extends Number> numbers = new ArrayList<Integer>();
        numbers =  new ArrayList<Double>();

//        numbers.add(1);
//        numbers.add(1.0);
//        numbers.add(1L);
//        numbers.add(1.0f);
        numbers.add(null);


    }
}
