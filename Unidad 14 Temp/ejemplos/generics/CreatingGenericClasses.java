package ejemplos.generics;

import java.util.ArrayList;
import java.util.List;

public class CreatingGenericClasses {
    public static void main(String[] args) {
        Create<String> createStr = new Create<>();
        Create<String> createInt = new Create<>();
        Create<Object> createObj = new Create<>();
    }
}

class Create<T> {
    List<T> data = new ArrayList<>();
    boolean add(T t) {return data.add(t);}
    T remove(T t) {data.remove(t); return t;}
    void print() {data.forEach(System.out::println);}
}