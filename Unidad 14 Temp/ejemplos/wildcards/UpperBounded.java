package ejemplos.wildcards;

import java.util.Collection;
import java.util.HashSet;

public class UpperBounded {

    private static abstract class Shape {}
    private static abstract class Shape2D extends Shape {}

    private static final class Circle extends Shape2D {}
    private static final class Square extends Shape2D {}

    public static void main(String[] args) {
        //TODO -> DECIMOS QUE Shape2D ES EL LIMITE SUPERIOR DEL WILDCARD
        Collection<? extends Shape2D> shape2DS ;

        //TODO -> AL UTILIZAR EL UPPER BOUNDED WILDCARD, EL TIPO DE LA COLECCION PUEDE SER TANTO EL MISMO Shape2D COMO: Circle O Square.
        //TODO -> POR LO QUE LAS SIGUIENTES INICIALIZACIONES SON PERMITIDAS
        shape2DS = new HashSet<Shape2D>();
        shape2DS = new HashSet<Circle>();
        shape2DS = new HashSet<Square>();

        //TODO -> PERO UN SUPERTIPO DE Shape2D NO ES PERMITIDO
//        shape2DS = new HashSet<Shape>();

        //TODO -> CON ESTE TIPO DE WILDCARD NO PODRIAMOS AGREGAR ELEMENTOS A LA LISTA.
        //TODO -> SI LA LISTA ES INICIALIZADA POR EJEMPLO CON: new HashSet<Circle>() Y TRATAMOS DE AGREGAR
        //TODO -> EN ELLA: shape2DS.add(new Shape2D() {}); NO CABRIA PORQUE Circle ES HIJO DE Shape2D Y UN PADRE NO CABE EN UN HIJO.
//        shape2DS.add(new Shape2D() {});   //ERROR DE COMPILACION
//        shape2DS.add(new Circle() {});    //ERROR DE COMPILACION
//        shape2DS.add(new Square() {});    //ERROR DE COMPILACION

        //todo -> SE DICE QUE LA LISTA ES IMMUTABLE DEBIDO A QUE NO SE PUEDEN AGREGAR ELEMENTOS, LO QUE NO ES CIERTO.
        shape2DS.add(null); //AGREGA UN null A LA LISTA
        shape2DS.add(null); //AGREGA UN null A LA LISTA

    }
}