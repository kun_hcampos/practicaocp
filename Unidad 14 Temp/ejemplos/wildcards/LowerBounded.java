package ejemplos.wildcards;

import java.util.Collection;
import java.util.HashSet;

public class LowerBounded {
    private static abstract class Shape {}
    private static abstract class Shape2D extends Shape {}

    private static final class Circle extends Shape2D{}
    private static final class Square extends Shape2D{}

    public static void main(String[] args) {
        //TODO -> DECIMOS QUE Shape2D ES EL LIMITE INFERIOR DEL WILDCARD
        Collection<? super Shape2D> shape2DS ;

        //TODO -> AL UTILIZAR EL LOWER BOUNDED WILDCARD, EL TIPO DE LA COLECCION PUEDE SER TANTO EL MISMO Shape2D COMO SU SUPERTIPO Shape U Object.
        shape2DS = new HashSet<Shape2D>();
        shape2DS = new HashSet<Shape>();
        shape2DS = new HashSet<Object>();

        //TODO -> EL LOWER WILDCARD PERMITE AGREGAR ELEMENTOS EN LA LISTA, PERO EL MINIMO TIPO QUE PERMITE ES EL ESPECIFICADO EN EL WILDCARD.
        //TODO -> POR EJEMPLO, SI LA LISTA SE INICIALIZA CON: new HashSet<Shape2D>(); NO PODRIAMOS AGREGAR UN TIPO Shape: shape2DS.add(new Shape(){}); PORQUE NO CABE UN PADRE DENTRO DE UN HIJO.
        shape2DS = new HashSet<Shape2D>();
//        shape2DS.add(new Shape(){});

        //TODO -> PERO SI SE PUEDE AGREGAR CUALQUIER SUBTIPO DEL DEFINIDO EN EL WILDCARD.
        shape2DS.add(new Circle());
        shape2DS.add(new Square());
    }

}
