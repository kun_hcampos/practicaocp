package ejemplos.comparing;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorExample {
    private int age;
    public ComparatorExample(int age){this.age = age; }

    public static void main(String[] args) {


        List list =Arrays.asList("c","a","ddd","d","b","dd");
        Comparator<String> c = Comparator.comparing(String::toString)
                .thenComparingInt(String::length);

        list.sort(c.thenComparing(java.util.Comparator.naturalOrder()));
        System.out.println(list);
        Arrays.asList(13,1,20,5).sort(Comparator.comparingInt(Integer::intValue));
        Arrays.asList(13L,1L,20L,5L).sort(Comparator.comparingLong(Long::longValue));
        Arrays.asList(13D,1D,20D,5D).sort(Comparator.comparingDouble(Double::doubleValue));




    }
}