package ejemplos.comparing;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BinarySearch {
    public static void main(String[] args) {
        var names = Arrays.asList("Lulu","Fluffy", "Hoppy");
        Comparator<String> c = Comparator.reverseOrder();
        var index = Collections.binarySearch(names, "Hoppy",c);
        System.out.println(index);
    }
}
