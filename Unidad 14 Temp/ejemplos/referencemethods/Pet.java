package referencemethods;

import java.time.LocalDate;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Pet {

    public static void main(String[] args) {
        //STATIC METHOD REFERENCE
        Consumer<Pet> print = Pet::printPet;

        Pet p = new Pet(LocalDate.now(), "LULU");
        print.accept(p);

        // INTANCE METHOD ON A SPECIFIC OBJECT
        Supplier<String> name = p::getName;
        System.out.println(name.get());

        // INSTANCE METHOD ON A PARAMETER
        Function<Pet, LocalDate> getBirthday = Pet::getBirthday;
        System.out.println(getBirthday.apply(p));

        //  CONSTRUCTOR REFERENCES
        BiFunction<LocalDate, String, Pet> petBuilder = Pet::new;
        var newPet = petBuilder.apply(LocalDate.now(), "COPITO");
    }

    // INSTANCE FIELDS
    private LocalDate birthday;
    private String name;

    //  CONSTRUCTOR
    public Pet(LocalDate birthday, String name) {
        this.birthday = birthday;
        this.name = name;
    }

    //  GETTERS ...
    public LocalDate getBirthday() {return birthday;}
    public String getName() {return name;}

    // STATIC PRINT METHOD
    public static void printPet(Pet pet){
        System.out.println(pet.getName().concat(" born on ").concat(pet.getBirthday().toString()));
    }
}
