package StreamExamples;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamSourcesExample {

    public static void main(String[] args) {
        Stream<String> fromArray = Stream.of("ab", "bbbb", "cdddddd","aa","aa");
       // Stream<Integer> singleElement = Stream.of(1);

       Stream<Double> randoms = Stream.generate(Math::random);
        //long c= Stream.generate(() -> 1.0).count();
        //Stream<Integer> oddNumbers = Stream.iterate(1, n -> n + 2);
        //randoms.forEach(System.out::println);
        //fromArray.collect(Collectors.groupingBy(x->x.length())).entrySet().forEach(System.out::println);
        //fromArray.collect(Collectors.partitioningBy(x->x.contains("a"))).entrySet().forEach(System.out::println);
        fromArray.collect(Collectors.partitioningBy(x->x.contains("a"), Collectors.toList())).
                entrySet().forEach(System.out::println);

        // System.out.println(fromArray.collect(Collectors.groupingBy((x)->x.length())));


        // oddNumbers.forEach(System.out::println);

        //numeros impares
       /* Stream<Integer> oddNumberUnder100 = Stream.iterate(
                1, // seed
                n -> n < 100, // Predicate to specify when done
                n -> n + 2);
        oddNumberUnder100.forEach(System.out::println);*/

    }
}
