package StreamExamples;

import java.util.List;
import java.util.stream.Stream;

public class StreamIntermediateExample {

    public static void main(String[] args) {
       // flatMapExample();
        /*Stream<String> two = Stream.of("aa","bbb","Mama Gorilla", "Baby Gorilla");
        two.peek(x->System.out.println(x))
                .filter(x->x.length()<=3).peek(System.out::println).count();*/
        flatMapExample2();
    }

    public static void flatMapExample()
    {
        List<String> zero = List.of();
        var one = List.of("Bonobo",null);
        var two = List.of("Mama Gorilla", "Baby Gorilla");
        Stream<List<String>> animals = Stream.of(zero, one, two);
        animals.flatMap(m -> m.stream())
                .forEach(System.out::println);
    }

    public static void flatMapExample2(){
        long count = Stream.of("Ken", "Jeff", "Ellen")
                .map(name -> name.chars())
                .flatMap(intStream -> intStream.mapToObj(n -> (char)n))
                .filter(ch -> ch == 'e' || ch == 'E')
                .count();
        System.out.println(count);
    }
}
