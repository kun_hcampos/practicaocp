package FunctionalInterfacesExamples;

import java.util.function.*;

public class FunctionInterfaceExamples {

    public static void main(String[] args) {

        functionExample();
        //biFunctionExample();

        Function<String,Boolean> function= x-> false;
        Predicate<String> predicate= x-> false;
    }

    public static void functionExample(){
        Function<String, Integer> f1 = String::length;
        Function<String, Integer> f2 = x -> x.length();
        System.out.println(f1.apply("cluck")); // 5
        System.out.println(f2.apply("cluck")); // 5
    }

    public static void biFunctionExample(){
        BiFunction<String, String, String> b1 = String::concat;
        BiFunction<String, String, String> b2 =
                (string, toAdd) -> string.concat(toAdd);
        System.out.println(b1.apply("baby ", "chick")); // baby chick
        System.out.println(b2.apply("baby ", "chick")); // baby chick
    }



    //Hereda de Interfaz funcional Fuction<T,R>
    public static void unaryOperatorExample(){

        UnaryOperator<String> u1 = String::toUpperCase;
        UnaryOperator<String> u2 = x -> x.toUpperCase();
        System.out.println(u1.apply("chirp")); // CHIRP
        System.out.println(u2.apply("chirp")); // CHIRP
    }

    //Hereda de Interfaz funcional BiFuction<T,T,R>
    public static void binaryOperatorExample(){

        BinaryOperator<String> b1 = String::concat;
        BinaryOperator<String> b2 = (string, toAdd) -> string.concat(toAdd);
        System.out.println(b1.apply("baby ", "chick")); // baby chick
        System.out.println(b2.apply("baby ", "chick")); // baby chick
    }

}
