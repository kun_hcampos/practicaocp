package FunctionalInterfacesExamples;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MetodosPorConvenienciaExample {

    public static void main(String[] args) {
        functionExample();
    }

    public static void predicateExample(){
        Predicate<String> egg = s -> s.contains("egg") && s.contains("brown");
        Predicate<String> brown = s -> s.contains("brown");

        Predicate<String> brownEggs =
                s -> s.contains("egg") && s.contains("brown");
        Predicate<String> otherEggs =
                s -> s.contains("egg") && ! s.contains("brown");

        Predicate<String> brownEggsAnd = egg.and(brown);
        Predicate<String> and = s -> s.contains("egg") && s.contains("brown");
        Predicate<String> otherEggsAnd = egg.and(brown.negate());
    }

    public static void consumerExample(){
        Consumer<String> c1 = x -> System.out.print("1: " + x);
        Consumer<String> c2 = x -> System.out.print(",2: " + x);
        Consumer<String> combined = c1.andThen(c2);
        combined.accept("Annie"); // 1: Annie,2: Annie
    }

    public static void functionExample(){

        Function<Integer, Integer> before = x -> x + 1;

        Function<Integer, Integer> after = x -> x * 2;

        Function<Integer,Integer> fAndThen= before.andThen(after);

        //System.out.println(fAndThen.apply(2)) //6;

        //el compose primero ejectua el before( 3+1=4) y luego el after(4*2) = 8
        Function<Integer, Integer> combined = before.compose(after);
       System.out.println(combined.apply(2)); // 5
    }
}
