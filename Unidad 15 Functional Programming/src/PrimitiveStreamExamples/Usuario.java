package PrimitiveStreamExamples;

import java.math.BigDecimal;

public class Usuario {

    Long id;
    String nombre;
    Integer edad;
    Double salario;
    BigDecimal otroValor;

    public Usuario(Long id, String nombre, Integer edad, Double salario) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.salario = salario;
        this.otroValor=BigDecimal.valueOf(id);
    }

    public BigDecimal getOtroValor() {
        return otroValor;
    }

    public void setOtroValor(BigDecimal otroValor) {
        this.otroValor = otroValor;
    }
}
