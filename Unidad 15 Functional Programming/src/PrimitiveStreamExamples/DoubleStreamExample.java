package PrimitiveStreamExamples;

import java.util.OptionalInt;
import java.util.function.*;
import java.util.stream.DoubleStream;

public class DoubleStreamExample {

    public static void main(String[] args) {
        var random = DoubleStream.generate(Math::random);
        var fractions = DoubleStream.iterate(.5, d -> d / 2);
        random.limit(3).forEach(System.out::println);
        fractions.limit(3).forEach(System.out::println);

        DoubleStream doubleStream = DoubleStream.of(2,3,4);
        //OptionalInt
    }



}
