package PrimitiveStreamExamples;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StreamPrimitiveExample {

    public static void main(String[] args) {
        List<Usuario> usuarios= new ArrayList<>();
        usuarios.add(new Usuario(1L,"prueba",30,2000.20));
        usuarios.add(new Usuario(2L,"prueba",20,1000.20));
        usuarios.add(new Usuario(3L,"prueba",60,900.20));
        usuarios.add(new Usuario(4L,"prueba",25,500.20));

        usuarios.stream().map(usuario -> usuario.salario).mapToDouble(x->x).average();

        usuarios.stream().mapToDouble(usuario -> usuario.salario).
                summaryStatistics().getAverage();
        usuarios.stream().map(x->x.otroValor).reduce(BigDecimal.ZERO,BigDecimal::add);

    }
}
