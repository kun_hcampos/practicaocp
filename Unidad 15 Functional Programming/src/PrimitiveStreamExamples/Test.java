package PrimitiveStreamExamples;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

//IntStream: Used for the primitive types int, short, byte, and char
//LongStream: Used for the primitive type long
//DoubleStream: Used for the primitive types double and float

public class Test {

    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1, 2, 3);
        System.out.println(stream.reduce(0, (s, n) -> s + n)); // 6

        LongStream longStream= LongStream.range(0,100);
        System.out.println( longStream.count());

        IntStream intStream= IntStream.range(0,42);
        intStream.summaryStatistics().getMin();
        intStream.summaryStatistics().getMax();
        intStream.summaryStatistics().getAverage();

        Stream<Long> stream2 = Arrays.asList(1l, 2l, 3l).stream();
        //stream2.mapToLong(x->x)
        System.out.println(stream2.mapToLong(x -> x).sum()); // 6
    }


    public static void intPrimitiveExample(){
        IntStream intStream = IntStream.of(1, 2, 3);
        OptionalDouble avg = intStream.average();
        System.out.println(avg.getAsDouble()); // 2.0
    }

    public static void doublePrimitiveExample(){
        DoubleStream oneValue = DoubleStream.of(3.14);
        oneValue.forEach(System.out::println);
        DoubleStream varargs = DoubleStream.of(1.0, 1.1, 1.2);
        varargs.forEach(System.out::println);
    }
}
